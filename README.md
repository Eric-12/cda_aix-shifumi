# CDA_Aix-Shifumi



## Getting started

Pour la réalisation du projet (html css et js) j'ai utilisé le speudo code réalisé en groupe ainsi que les enchainements d'écran (parcours utilisateur).

Je n'ai pas utilisé la méthode ou l'on masque les blocs via une règle contenant display:none.
J'ai préféré utiliser les backtiks ce qui m'a permis d'injecté des blocs html directement dans un conteneur avec la propriété "innerHtml".